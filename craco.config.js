const fs = require('fs');
const path = require('path');
class WebpackPathOverridePlugin {
    constructor(pathRegExp, pathReplacement, exts) {
        this.pathRegExp = pathRegExp;
        this.pathReplacement = pathReplacement;
        this.exts = exts;
    }

    getResolvedFile(filePath, exts, callback) {
        var enclosingDirPath = filePath || '';
        var extObjs = exts.reduce(function (allExts, ext) {
            allExts.push(
                { ext: ext, file: true },
                { ext: ext, file: false }
            )
            return allExts
        }, [])

        var tryToFindExtension = function (index) {
            var extObj = extObjs[index];
            // None of passed extensions are found
            if (!extObj) {
                return callback(false);
            }
            var componentFileName, componentFilePath;
            // Try to load regular file
            if (extObj.file) {
                componentFilePath = enclosingDirPath;
                var extension = '.' + extObj.ext;
                if (componentFilePath.slice(extension.length * -1) !== extension) {
                    componentFilePath += extension;
                }
            } else {
                componentFileName = enclosingDirPath + '.' + extObj.ext;
                componentFilePath = path.join(enclosingDirPath, componentFileName);
            }
            fs.stat(componentFilePath, function (err, stats) {
                if (err || !stats.isFile()) {
                    return tryToFindExtension(index + 1);
                }
                callback(componentFilePath)
            });
        };
        tryToFindExtension(0);
    }

    overrideRequestPath(result) {
        const newResult = { ...result };
        const pathRegExp = this.pathRegExp;
        const pathReplacement = this.pathReplacement;

        const { request, dependencies } = newResult;
        newResult.request = request.replace(pathRegExp, pathReplacement);

        if (dependencies) {
            dependencies.forEach((dependency) => {
                const dependencyRequest = dependency.request;
                if (dependencyRequest) {
                    dependency.request = dependencyRequest.replace(pathRegExp, pathReplacement);
                }
            })
        }

        return newResult;
    }

    apply(resolver) {
        resolver.plugin('normal-module-factory', (nmf) => {
            nmf.plugin('before-resolve', (result, callback) => {
                if (!result) return callback();
                const overrideRequestPath = result => this.overrideRequestPath(result);

                // test the request for a path match
                if (this.pathRegExp.test(result.request)) {
                    const pathToTest = './src/' + result.request.replace(this.pathRegExp, this.pathReplacement);
                    this.getResolvedFile(pathToTest, this.exts, function (file) {
                        if (typeof file === 'string') {
                            const newResult = overrideRequestPath(result);
                            return callback(null, newResult);
                        }
                        return callback(null, result);
                    })
                } else {
                    return callback(null, result);
                }
            });
        });
    }
};

module.exports = {
    webpack: {
        plugins: [
            new WebpackPathOverridePlugin(/^core/, 'app', ['jsx', 'js', 'tsx', 'ts'])
        ]
    }
}