import React from 'react';
import First from 'core/components/first/index';
import Second from 'core/components/second/index';
import Third from 'core/components/third/index';
import './App.css';

function App() {
  return (
    <div className="App">
      <First />
      <Second displayName="Second"/>
      <Third />
    </div>
  );
}

export default App;
