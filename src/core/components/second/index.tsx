import React from 'react';
import Second from 'core/components/second/display';

interface Props {
    displayName: string
}

export interface DisplayProps {
    displayName: string
}

const SecondContainer: React.FC<Props> = ({displayName}) => {
    return (
        <div>
            <Second displayName={displayName} />
        </div>
    )
}

export default SecondContainer;