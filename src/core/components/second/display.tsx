import React from 'react';

import { DisplayProps as Props } from 'core/components/second/index';

const Second: React.FC<Props> = ({displayName}) => {
    return (
        <div>
            {displayName} from core!
        </div>
    )
}

export default Second
